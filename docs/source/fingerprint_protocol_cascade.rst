Fingerprint protocol cascade
============================

^^^^^^^^^^^^^^^^^^^^
Accumulation manager
^^^^^^^^^^^^^^^^^^^^

.. automodule:: accumulation_manager
   :members:

^^^^^^^^^^^
Accumulator
^^^^^^^^^^^

The accumulator module contains the main routine used to compute the accumulation process, which
is an essential part of both the second and third fingerprint protocol.

.. automodule:: accumulator
   :members:

^^^^^^^^
Assigner
^^^^^^^^

.. automodule:: assigner
   :members:

^^^^^^^
Trainer
^^^^^^^

Trains an instance of the class :class:`~id_CNN.ConvNetwork`

.. automodule:: trainer
  :members:

^^^^^^^^^^^
Pretraining
^^^^^^^^^^^

Pretrains the network as the first step of the third fingerprint protocol.

.. automodule:: pre_trainer
  :members:
