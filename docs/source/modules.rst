Code documentation
==================
Contents:

.. toctree::
   :maxdepth: 2

  video_information_manager
  preprocessing
  crossing_detector
  fragmentation
  idCNN
  fingerprint_protocol_cascade
  postprocessing
  validation
