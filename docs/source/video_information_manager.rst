Video information manager
=========================

Collects information concerning the video (fps, number of channels, ...) and manages
the creation of the folders used to save and load both partial and final outputs of
the algorithm

.. automodule:: video
  :members:
